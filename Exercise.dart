import 'dart:io';

void main() {
  print("choice you want to enter");
  int? choice = int.parse(stdin.readLineSync()!);

  switch (choice) {
    case 1:
      {
        print("Enter the value for x : ");
        int? x = int.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int sum = 0;
        sum = x + y;
        print("Sum of the number is : ");
        print(sum);
      }
      break;
    case 2:
      {
        print("Enter the value for x : ");
        int? x = int.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int sum = 0;
        sum = x - y;
        print("Sum of the number is : ");
        print(sum);
      }
      break;
    case 3:
      {
        print("Enter the value for x : ");
        int? x = int.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int sum = 0;
        sum = x * y;
        print("Sum of the number is : ");
        print(sum);
      }
      break;
    case 4:
      {
        print("Enter the value for x : ");
        int? x = int.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int sum = 0;
        sum = (x / y).toInt();
        print("Sum of the number is : ");
        print(sum);
      }
      break;
    case 5:
      {
        print("Exit");
      }
      break;
    default:
      break;
  }
}
