//ให้รับประโยคหรือคำโดย 1. ความยาวของเซต 2.ไม่นับตัวซ้ำ 3.ในsetมีคำซ้ำกี่คำ
import 'dart:io';

void main() {
  String str = stdin.readLineSync()!.toLowerCase();
  print(str);
  var arr = str.split(" ");

  print("number of word: "); //แสดงว่ามีกี่คำ
  var CountList = arr.toSet().toList();
  print(CountList.length);

  var coutWord = Map(); //แทรกkey
  arr.forEach((element) {
    //วนloopแต่ละคำใน arr
    if (!coutWord.containsKey(element)) {
      //
      coutWord[element] = 1; //+1
    } else {
      coutWord[element] += 1; //ถ้ามีหลายตัว+1เรื่อยๆ
    }
  });
  print("number of word : ");
  print(coutWord);
}
